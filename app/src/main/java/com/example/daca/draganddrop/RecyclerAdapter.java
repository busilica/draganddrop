package com.example.daca.draganddrop;

import android.support.v4.view.MotionEventCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.example.daca.draganddrop.helper.ItemTouchContract;
import com.example.daca.draganddrop.helper.SimpleItemTouchHelperCallback;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Daca on 28.02.2017..
 */

public class RecyclerAdapter extends RecyclerView.Adapter<ViewHolder> implements ItemTouchContract.ItemTouchHelperAdapter,
        ItemTouchContract.OnStartDragLIstener {

    private LayoutInflater inflater;
    private ArrayList<Model> list;
    private ItemTouchContract.OnStartDragLIstener dragStartLitener;
    private ItemTouchHelper touchHelper;

    public RecyclerAdapter(LayoutInflater inflater, RecyclerView recyclerView) {
        this.inflater = inflater;
        list = new ArrayList<>();
        generateList(list);
        ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(this);
        touchHelper = new ItemTouchHelper(callback);
        touchHelper.attachToRecyclerView(recyclerView);
        dragStartLitener = this;
    }

    private void generateList(ArrayList<Model> list) {
        for (int i = 0; i < 10; i++){
            list.add(new Model("Text" + i, "text" + i));
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.custom_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
        Model model = list.get(position);
        holder.tv1.setText(model.getTv1());
        holder.tv2.setText(model.getTv2());
        holder.drag.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (MotionEventCompat.getActionMasked(motionEvent) ==
                        MotionEvent.ACTION_DOWN){
                    dragStartLitener.onStartDrag(holder);
                }
                return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        if (fromPosition < toPosition){
            for (int i = fromPosition; i < toPosition; i++){
                Collections.swap(list, i, i + 1);
            }
        }else {
            for (int i = fromPosition; i > toPosition; i--){
                Collections.swap(list, i, i - 1);
            }
        }
        notifyItemMoved(fromPosition, toPosition);
        return true;
    }

    @Override
    public void onItemDismiss(int position) {
        list.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
        touchHelper.startDrag(viewHolder);
    }
}
