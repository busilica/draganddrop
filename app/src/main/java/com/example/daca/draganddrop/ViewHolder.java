package com.example.daca.draganddrop;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.daca.draganddrop.helper.ItemTouchContract;

/**
 * Created by Daca on 28.02.2017..
 */

public class ViewHolder extends RecyclerView.ViewHolder implements ItemTouchContract.ItemTouchHelperViewHolder {

    TextView tv1, tv2;
    ImageView drag;

    public ViewHolder(View itemView) {
        super(itemView);
        tv1 = (TextView) itemView.findViewById(R.id.tv1_custom_row);
        tv2 = (TextView) itemView.findViewById(R.id.tv2_custom_row);
        drag = (ImageView) itemView.findViewById(R.id.btn_drag_custom_row);
    }

    @Override
    public void onItemSelected() {
        itemView.setBackgroundColor(Color.BLUE);
    }

    @Override
    public void onItemClear() {
        itemView.setBackgroundColor(Color.parseColor("#ffffff"));
    }
}
