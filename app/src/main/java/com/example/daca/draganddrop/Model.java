package com.example.daca.draganddrop;

/**
 * Created by Daca on 28.02.2017..
 */

public class Model {

    String tv1;
    String tv2;

    public Model(String tv1, String tv2) {
        this.tv1 = tv1;
        this.tv2 = tv2;
    }

    public String getTv1() {
        return tv1;
    }

    public void setTv1(String tv1) {
        this.tv1 = tv1;
    }

    public String getTv2() {
        return tv2;
    }

    public void setTv2(String tv2) {
        this.tv2 = tv2;
    }
}
