package com.example.daca.draganddrop.helper;

import android.support.v7.widget.RecyclerView;

/**
 * Created by Daca on 28.02.2017..
 */

public interface ItemTouchContract {

    interface ItemTouchHelperAdapter {
        /**
         * Called when an item has been dragged far enough to trigger a move. This is called
         * every time an item is shifted
         */
        boolean onItemMove(int fromPosition, int toPosition);

        //Called when a item has been dismissed by a swipe
        void onItemDismiss(int position);
    }

    interface ItemTouchHelperViewHolder {
        //Called the item first registers as being moved or swiped
        void onItemSelected();
        //Called when the move or swipe is completed, and active item should
        //be cleared
        void onItemClear();
    }

    interface OnStartDragLIstener {
        //Called when a view is requesting a start of a drag.
        void onStartDrag(RecyclerView.ViewHolder viewHolder);
    }
}
